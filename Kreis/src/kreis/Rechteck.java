package kreis;

import java.awt.Point;

public class Rechteck {
	
	private double hoehe;
	private double breite;
	private Point mittelpunkt;
	
	public Rechteck(double hoehe, double breite, Point mittelpunkt) {
		setHoehe(hoehe);
		setBreite(breite);
		setMittelpunkt(mittelpunkt);
	}
	
	// Umfang
	public double getUmfang() {
		return 2 * this.breite + 2 * this.hoehe;
	}
	
	// Diagonale
	public double getDiagonale() {
		return Math.sqrt((this.breite * this.breite) + (this.hoehe * this.hoehe));
	}
	
	// Flaeche
	public double getFlaeche() {
		return this.hoehe * this.breite;
	}
	
	// Hoehe
	public double getHoehe() {
		return hoehe;
	}

	public void setHoehe(double hoehe) {
		if(hoehe > 0) {
			this.hoehe = hoehe;
		} else {
			this.hoehe = 0;
		}
	}

	// Breite
	public double getBreite() {
		return breite;
	}

	public void setBreite(double breite) {
		if(breite > 0) {
			this.breite = breite;
		} else {
			this.breite = 0;
		}
	}

	// Mittelpunkt
	public Point getMittelpunkt() {
		return mittelpunkt;
	}

	public void setMittelpunkt(Point mittelpunkt) {
		this.mittelpunkt = mittelpunkt;
	}

}
