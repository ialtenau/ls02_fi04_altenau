package kreis;

import java.awt.Point;

public class Testrechteck {

	public static void main(String[] args) {
		Rechteck r = new Rechteck(5, 10, new Point(3,4));
		System.out.println();
		System.out.println("Diagonale: " + r.getDiagonale());
		System.out.println("Flaeche: " + r.getFlaeche());
		System.out.println("Umfang: " + r.getUmfang());
	}

}
