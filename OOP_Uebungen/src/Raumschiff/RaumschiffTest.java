package Raumschiff;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * Klasse zum Testen der Raumschiff und Ladungsklassen
 * 
 * @author i.altenau
 *
 */
public class RaumschiffTest {

	public static void main(String[] args) {
		// Klingonen
		Ladung ladungKlingonen1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung ladungKlingonen2 = new Ladung("Bat'leth Klingonen Schwert", 200);
		
		List<Ladung> ladungKlingonen = new ArrayList<Ladung>();
		ladungKlingonen.add(ladungKlingonen1);
		ladungKlingonen.add(ladungKlingonen2);
		
		Raumschiff klingonen = new Raumschiff(
					1,
					100,
					100,
					100,
					100,
					2,
					"IKS Hegh'ta",
					new ArrayList<String>(),
					ladungKlingonen
		);
		
		// Romulaner
		Ladung ladungRomulaner1 = new Ladung("Borg-Schrott", 5);
		Ladung ladungRomulaner2 = new Ladung("Rote Materie", 2);
		Ladung ladungRomulaner3 = new Ladung("Plasma-Waffe", 50);
		
		List<Ladung> ladungRomulaner = new ArrayList<Ladung>();
		ladungRomulaner.add(ladungRomulaner1);
		ladungRomulaner.add(ladungRomulaner2);
		ladungRomulaner.add(ladungRomulaner3);
		
		Raumschiff romulaner = new Raumschiff(
					2,
					100,
					100,
					100,
					100,
					2,
					"IRW Khazara",
					new ArrayList<String>(),
					ladungRomulaner
		);
		
		// Vulkanier
		Ladung ladungVulkanier1 = new Ladung("Forschungssonde", 35);
		Ladung ladungVulkanier2 = new Ladung("Photonentorpedo", 3);
		
		List<Ladung> ladungVulkanier = new ArrayList<Ladung>();
		ladungVulkanier.add(ladungVulkanier1);
		ladungVulkanier.add(ladungVulkanier2);

		Raumschiff vulkanier = new Raumschiff(
					0,
					80,
					80,
					50,
					100,
					5,
					"Ni'Var",
					new ArrayList<String>(),
					ladungVulkanier
		);
		
		// Methodenaufrufe
		klingonen.photonentorpedosAbschießen(romulaner);
		romulaner.phaserkanoneAbschießen(klingonen);
		vulkanier.nachrichtVersenden("Gewalt ist nicht logisch");
		klingonen.zustandAusgeben();
		klingonen.ladeverzeichnisAusgeben();
		vulkanier.reparaturAndroidenEinzetzen(5,
				(vulkanier.getEnergieversorgungInProzent() < 100),
				(vulkanier.getSchildeInProzent() < 100),
				(vulkanier.getLebenserhaltungssystemeInProzent() < 100),
				(vulkanier.getHuelleInProzent() < 100)
				);
		vulkanier.photonentorpedosEinsetzen(3);
		vulkanier.ladungsverzeichnisAufraeumen();
		klingonen.photonentorpedosAbschießen(romulaner);
		klingonen.photonentorpedosAbschießen(romulaner);
		
		System.out.println("");
		klingonen.zustandAusgeben();
		klingonen.ladeverzeichnisAusgeben();
		System.out.println("");
		romulaner.zustandAusgeben();
		romulaner.ladeverzeichnisAusgeben();
		System.out.println("");
		vulkanier.zustandAusgeben();
		vulkanier.ladeverzeichnisAusgeben();

	}

}
