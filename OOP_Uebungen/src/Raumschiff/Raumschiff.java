package Raumschiff;

import java.util.List;

/**
 * Klasse fuer Raumschiffobjekte
 * 
 * @author i.altenau
 */
public class Raumschiff {
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private List<String> broadcastCommunicator;
	private List<Ladung> ladung;
	
	/**
	 * Konstruktor leer
	 */
	public Raumschiff() {
		
	}

	/**
	 * Konstruktor mit Parametern
	 * 
	 * @param photonentorpedoAnzahl Photonentorpedoanzahl
	 * @param energieversorgungInProzent Energieversorgung in Prozent
	 * @param schildeInProzent Schilde in Prozent
	 * @param huelleInProzent Huelle in Prozent
	 * @param lebenserhaltungssystemeInProzent Lebenererhaltungssysteme in Prozent
	 * @param androidenAnzahl Androidenanzahl
	 * @param schiffsname Schiffsname
	 * @param broadcastCommunicator Broadcastcommunicatorliste
	 * @param ladung Ladungsliste
	 */
	public Raumschiff(
			int photonentorpedoAnzahl,
			int energieversorgungInProzent,
			int schildeInProzent,
			int huelleInProzent,
			int lebenserhaltungssystemeInProzent,
			int androidenAnzahl,
			String schiffsname,
			List<String> broadcastCommunicator,
			List<Ladung> ladung
			) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
		this.broadcastCommunicator = broadcastCommunicator;
		this.ladung = ladung;
	}
	
	// addLadung
	/**
	 * laedt die Ladung ins Ladungsverzeichnis
	 * 
	 * @param ladung Ladung
	 * @return void
	 */
	public void beladen(Ladung ladung) {
		this.ladung.add(ladung);
	}

	/**
	 * Gibt alle Attribute zurueck (den Zustand des Raumschiffes)
	 * 
	 * @return void
	 */
	public void zustandAusgeben() {
		System.out.println("Raumschiff photonentorpedoAnzahl=" + this.photonentorpedoAnzahl);
		System.out.println("energieversorgungInProzent="+ this.energieversorgungInProzent);
		System.out.println("schildeInProzent=" + this.schildeInProzent);
		System.out.println("huelleInProzent="+ this.huelleInProzent);
		System.out.println("lebenserhaltungssystemeInProzent=" + this.lebenserhaltungssystemeInProzent);
		System.out.println("androidenAnzahl=" + this.androidenAnzahl);
		System.out.println("schiffsname=" + this.schiffsname);
		System.out.println("broadcastCommunicator="+ this.broadcastCommunicator);
	}
	
	/**
	 * Gibt das Ladungsverzeichnis in der Konsole aus
	 * @return void
	 */
	public void ladeverzeichnisAusgeben() {
		for(Ladung ladung: this.ladung){
			System.out.println("ladung="+ ladung);
		}
	}
	
	/**
	 * Schiesst Photonentorpedos auf den Gegner
	 * 
	 * @param raumschiff Raumschiff auf das geschossen wird
	 * @return void
	 */
	public void photonentorpedosAbschießen(Raumschiff raumschiff) {
		if (this.photonentorpedoAnzahl < 1) {
			this.nachrichtVersenden("-=*Click*=-");
		} else {
			this.photonentorpedoAnzahl--;
			this.nachrichtVersenden("Photonentorpedo abgeschossen");
			this.trefferVermerken(raumschiff);
		}
	}
	
	/**
	 * Schiesst Phaserkanone auf den Gegner
	 * 
	 * @param raumschiff Raumschiff auf das geschossen wird
	 * @return void
	 */
	public void phaserkanoneAbschießen(Raumschiff raumschiff) {
		if (this.energieversorgungInProzent < 50) {
			this.nachrichtVersenden("-=*Click*=-");
		} else {
			this.energieversorgungInProzent -= 50;
			this.nachrichtVersenden("Phaserkanone abgeschossen");
			this.trefferVermerken(raumschiff);
		}
	}
	
	/**
	 * vermerkt Treffer und rechnet Schaden aus
	 * 
	 * @param raumschiff Raumschiff das den Treffer vermerkt
	 * @return void
	 */
	private void trefferVermerken(Raumschiff raumschiff) {
		this.nachrichtVersenden(raumschiff.schiffsname + " wurde getroffen!");
		raumschiff.schildeInProzent -= 50;
		
		if(raumschiff.schildeInProzent < 1) {
			raumschiff.huelleInProzent -= 50;
			raumschiff.energieversorgungInProzent -= 50;
			
			if(raumschiff.huelleInProzent < 1) {
				raumschiff.lebenserhaltungssystemeInProzent = 0;
				this.nachrichtVersenden("Lebenserhaltungssysteme von " + raumschiff.schiffsname + " wurden vernichtet.");
			}
		}
	}
	
	// Nachricht an Alle
	/**
	 * Speichert eine Nachricht und gibt sie in der Konsole aus
	 * 
	 * @param nachricht Nachricht die gespeichert und in der Konsole ausgegeben werden soll
	 * @return void
	 */
	public void nachrichtVersenden(String nachricht) {
		this.broadcastCommunicator.add(nachricht);
		System.out.println(nachricht);
	}
	
	// Logbucheintraege ausgeben, da der broadcastKommunikator ja schon im getter geholt werden kann
	/**
	 * Gibt das Logbuch in der Konsole aus
	 * @return void
	 */
	public void logbuchZurueckgeben() {
		for(String nachricht: this.broadcastCommunicator){
			System.out.println(nachricht);
		}
	}
	
	/**
	 * setzt Photonentorpedos aus der Ladung in das Schiff
	 * 
	 * @param anzahl Anzahl der Photonentorpedos die eingesetzt werden sollen
	 * @return void
	 */
	public void photonentorpedosEinsetzen(int anzahl) {
		for(Ladung ladung: this.ladung){
			
			if (ladung.getBezeichnung() == "Photonentorpedo") {
				
				if (ladung.getMenge() < anzahl) {
					anzahl = ladung.getMenge();
				}
				
				ladung.setMenge(ladung.getMenge() - anzahl);
				this.photonentorpedoAnzahl += anzahl;
				System.out.println(anzahl + " Photonentorpedos eingesetzt");
				
				return;
			}
		}
		
		System.out.println("Keine Photonentorpedos gefunden!");
		this.nachrichtVersenden("-=*Click*=-");
	}
	
	/**
	 * Raeumt das Ladungsverzeichnis auf (loescht Ladung mit Menge < 1)
	 * @return void
	 */
	public void ladungsverzeichnisAufraeumen() {
		this.ladung.removeIf(ladung -> ladung.getMenge() < 1);
	}
	
	/**
	 * Setzt Reparaturandroiden ein um Schiff zu reparieren
	 * @param benoetigteAndroiden Benoetigte Androiden
	 * @param energieversorgungInProzent Energieversorgung in Prozent
	 * @param schildeInProzent Schilde in Prozent
	 * @param lebenserhaltungssystemeInProzent Lebenserhaltungssysteme in Prozent
	 * @param huelleInProzent Huelle in Prozent
	 * @return void
	 */
	public void reparaturAndroidenEinzetzen(
			int benoetigteAndroiden,
			boolean energieversorgungInProzent,
			boolean schildeInProzent,
			boolean lebenserhaltungssystemeInProzent,
			boolean huelleInProzent
			) {
        int rnd = (int) Math.round(Math.random() * 100);
        int reparaturBenoetigt = 0;
        benoetigteAndroiden = Math.min(getAndroidenAnzahl(), benoetigteAndroiden);
        this.setAndroidenAnzahl(getAndroidenAnzahl() - benoetigteAndroiden);

        if (energieversorgungInProzent) {
            reparaturBenoetigt++;
        }
        
        if (schildeInProzent) {
            reparaturBenoetigt++;
        }
        
        if (lebenserhaltungssystemeInProzent) {
            reparaturBenoetigt++;
        }
        
        if (huelleInProzent) {
            reparaturBenoetigt++;
        }

        int reparierenUm = rnd * benoetigteAndroiden / reparaturBenoetigt;

        if (energieversorgungInProzent) {
            this.setEnergieversorgungInProzent(Math.min(this.getEnergieversorgungInProzent() + reparierenUm, 100));
        }
        
        if (schildeInProzent) {
        	this.setSchildeInProzent(Math.min(this.getSchildeInProzent() + reparierenUm, 100));
        }
        
        if (lebenserhaltungssystemeInProzent) {
        	this.setLebenserhaltungssystemeInProzent(Math.min(this.getLebenserhaltungssystemeInProzent() + reparierenUm, 100));
        }
        
        if (huelleInProzent) {
        	this.setSchildeInProzent(Math.min(this.getSchildeInProzent() + reparierenUm, 100));
        }

    }
	
	/**
	 * getter fuer den BroadcastCommunicator
	 * @return broadcastCommunicator
	 */
	public List<String> getBroadcastCommunicator() {
		return broadcastCommunicator;
	}

	/**
	 * getter fuer die Photonentorpedoanzahl
	 * @return Anzahl der Photonentorpedos
	 */
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	/**
	 * setter fuer die Photonentorpedoanzahl
	 * @param photonentorpedoAnzahl Anzahl der Photonentorpedos
	 * @return void
	 */
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	/**
	 * getter fuer Energieversorgung in Prozent
	 * @return Energieversorgung in Prozent
	 */
	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	/**
	 * setter fuer Energieversorgung in Prozent
	 * @param energieversorgungInProzent Energieversorgung in Prozent
	 * @return void
	 */
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	/**
	 * getter fuer Schilde in Prozent
	 * @return Schilde in Prozent
	 */
	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	/**
	 * setter fuer Schilde in Prozent
	 * @param schildeInProzent Schilde in Prozent
	 * @return void
	 */
	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	/**
	 * getter fuer Huelle in Prozent
	 * @return Huelle in Prozent
	 */
	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	/**
	 * setter fuer Huelle in Prozent
	 * @param huelleInProzent Huelle in Prozent
	 * @return void
	 */
	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	/**
	 * getter fuer Lebenenerhaltungssysteme
	 * @return Lebenerhaltungssysteme in Prozent
	 */
	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	/**
	 * setter fuer Lebenserhaltungssysteme in Prozent
	 * @param lebenserhaltungssystemeInProzent Lebenserhaltungssysteme in Prozent
	 * @return void
	 */
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	/**
	 * getter fuer Androidenanzahl
	 * @return Anzahl der Androiden
	 */
	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	/**
	 * setter fuer AndroidenAnzahl
	 * @param androidenAnzahl Anzahl der Androiden
	 * @return void
	 */
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	/**
	 * getter fuer den Schiffsname
	 * @return Schiffsname
	 */
	public String getSchiffsname() {
		return schiffsname;
	}

	/**
	 * setter fuer den Schiffsnamen
	 * @param schiffsname Name des Schiffs
	 * @return void
	 */
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	/**
	 * setter fuer broadcastCommunicator
	 * @param broadcastCommunicator logbuchListe
	 * @return void
	 */
	public void setBroadcastCommunicator(List<String> broadcastCommunicator) {
		this.broadcastCommunicator = broadcastCommunicator;
	}

	/**
	 * getter fuer Ladungsliste
	 * @return Ladungsliste
	 */
	public List<Ladung> getLadung() {
		return ladung;
	}

	/**
	 * Setter fuer Ladung
	 * @param ladung Ladungsliste
	 * @return void
	 */
	public void setLadung(List<Ladung> ladung) {
		this.ladung = ladung;
	}
}
