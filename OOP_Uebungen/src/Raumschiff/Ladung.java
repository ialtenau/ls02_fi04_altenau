package Raumschiff;

/**
 * Klasse für Ladungsobjekte
 * 
 * @author i.altenau
 */
public class Ladung {
	private String bezeichnung;
	private int menge;
	
	/**
	 * Konstruktor leer
	 */
	public Ladung() {
	}
	
	/**
	 * Konstruktor mit Parameter
	 * 
	 * @param bezeichnung Name der Ladung
	 * @param menge Menge der Ladung
	 */
	public Ladung(String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}

	/**
	 * getter fuer Bezeichnung
	 * @return Bezeichnung der Ladung
	 */
	public String getBezeichnung() {
		return bezeichnung;
	}

	/**
	 * setter fuer Bezeichnung
	 * @param bezeichnung Bezeichnung der Ladung
	 * @return void
	 */
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	/**
	 * getter fuer Menge
	 * @return Menge der Ladung
	 */
	public int getMenge() {
		return menge;
	}

	/**
	 * setter fuer Menge
	 * @param menge Menge der Ladung
	 * @return void
	 */
	public void setMenge(int menge) {
		this.menge = menge;
	}

	@Override
	/**
	 * toString fuer die Ausgabe der Ladung
	 * @return void
	 */
	public String toString() {
		return "Ladung [bezeichnung=" + bezeichnung + ", menge=" + menge + "]";
	}
}
